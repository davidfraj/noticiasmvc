<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'Hello World !');

for($i=1;$i<=20;$i++){
	$sheet->setCellValue('A'.$i, 'Hello World !');
}


$writer = new Xlsx($spreadsheet);
$writer->save('hello world.xlsx');