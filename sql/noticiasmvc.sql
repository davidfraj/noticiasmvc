-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-04-2018 a las 20:14:54
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `noticiasmvc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `idNot` int(11) NOT NULL,
  `tituloNot` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `textoNot` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaNot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`idNot`, `tituloNot`, `textoNot`, `fechaNot`) VALUES
(2, 'Hoy estamos a miercoles 232333', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime necessitatibus esse nesciunt harum, totam quas, ex omnis porro earum! Autem numquam mollitia architecto iure perspiciatis quos, maxime eaque aliquam commodi. <br /> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime necessitatibus esse nesciunt harum, totam quas, ex omnis porro earum! Autem numquam mollitia architecto iure perspiciatis quos, maxime eaque aliquam commodi. 233333</p>\r\n<p><strong>HOLA</strong></p>\r\n<p><strong>Que tal </strong></p>', 1517958000),
(4, 'Ahora si... que la noticia de miercoles 22', 'Texto de la noticia de hoy miercoles 22', 1519858800),
(6, 'Feliz san valero', 'Texto de la noticia de San Valero', 1517353200),
(7, 'Explotara??', 'No lo se....', 1519254000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `idPedido` int(11) NOT NULL,
  `usuarioPedido` int(11) NOT NULL,
  `fechaPedido` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE `peliculas` (
  `idPel` int(11) NOT NULL,
  `tituloPel` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `textoPel` longtext COLLATE utf8_spanish_ci NOT NULL,
  `imagenPel` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `peliculas`
--

INSERT INTO `peliculas` (`idPel`, `tituloPel`, `textoPel`, `imagenPel`) VALUES
(1, 'El padrino', 'Sipnosis de la pelicula del el Padrino...', 'elpadrino.jpg'),
(2, 'Y si no nos enfadamos', 'PEdzao de pelicula de bud spencer y terence hill', 'enfadamos.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idPro` int(11) NOT NULL,
  `nombrePro` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `precioPro` double NOT NULL,
  `unidadesPro` int(11) NOT NULL,
  `fechaPro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idPro`, `nombrePro`, `precioPro`, `unidadesPro`, `fechaPro`) VALUES
(1, 'Impresora a color 2', 1202, 32, 1521068400),
(2, 'Portatil ACER', 780.5, 4, 1520875672),
(3, 'Raton BT', 34, 6, 1520809200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_pedidos`
--

CREATE TABLE `productos_pedidos` (
  `idProductoPedido` int(11) NOT NULL,
  `idPedido` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `cantidadProductoPedido` int(11) NOT NULL,
  `precioProductoPedido` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `correoUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `claveUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `nombreUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fechaUsuario` int(11) NOT NULL,
  `sessionUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estadoUsuario` int(1) NOT NULL DEFAULT '1',
  `nivelUsuario` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `correoUsuario`, `claveUsuario`, `nombreUsuario`, `fechaUsuario`, `sessionUsuario`, `estadoUsuario`, `nivelUsuario`) VALUES
(1, 'julio@gmail.com', '5e8667a439c68f5145dd2fcbecf02209', 'julio', 1521484817, '', 1, 9),
(2, 'david@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'david', 1522257358, '', 1, 1),
(3, 'davidfraj3@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'pedro', 1522257696, '', 1, 1),
(4, 'davidfraj@hotmail.com', '25d55ad283aa400af464c76d713c07ad', 'Raton BT', 1522257752, '', 1, 1),
(5, 'lunes@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'lunes', 1522685667, '', 1, 5),
(6, 'davidfraj@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'david', 1522946036, '', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idNot`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`idPedido`);

--
-- Indices de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`idPel`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idPro`);

--
-- Indices de la tabla `productos_pedidos`
--
ALTER TABLE `productos_pedidos`
  ADD PRIMARY KEY (`idProductoPedido`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `idNot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  MODIFY `idPel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idPro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `productos_pedidos`
--
ALTER TABLE `productos_pedidos`
  MODIFY `idProductoPedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
