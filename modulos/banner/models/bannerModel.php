
<?php  
class Banner{
	public $elementos;

	public function __construct(){
		$this->elementos=[];
	}

	public function add($image, $descripcion, $url){
		$elemento=new ElementoBanner($image, $descripcion, $url);
		$this->elementos[]=$elemento;
	}

	public function dimeAleatorio(){
		$inicio=0;
		$final=count($this->elementos)-1;
		return $this->elementos[rand($inicio, $final)];
	}

}


?>