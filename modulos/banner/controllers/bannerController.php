<?php  
//Fichero bannerController.php

//llamar al modelo
require('modulos/banner/models/elementoBannerModel.php');
require('modulos/banner/models/bannerModel.php');

//Genero los calculos necesarios
$banner=new Banner();
$banner->add('0.jpg', 'Gatito guay', 'http://www.google.es');
$banner->add('4.jpg', 'Otro Gatito guay', 'http://www.yahoo.es');
$banner->add('2.jpg', 'Gatito guay 2', 'http://www.google.uk');

//Genero la imagen a mostrar en la vista
$imagenBanner=$banner->dimeAleatorio();

//Llamar a la vista
require('modulos/banner/views/bannerView.php');

?>