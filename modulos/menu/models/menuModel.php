<?php  

class Menu{
	public $elementos;

	public function __construct(){
		$this->elementos=[]; //Le indico que es un array
	}

	public function add($titulo, $enlace){
		$elemento=new ElementoMenu($titulo, $enlace);
		$this->elementos[]=$elemento;
	}

	function traeElementos($contr){
		foreach ($this->elementos as $elemento) {
			if($elemento->enlace==$contr){
				$elemento->activado='active';
			}
		}
		return $this->elementos;
	}

}

?>