
<h3>
	 <small><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true" id="carrito"></span></small>
	 -
	Carrito de compra

</h3>
<hr>
<ul class="list-unstyled">
	<?php foreach ($lineasmicarro as $indice => $linea) { ?>
		
		<li>
			<?php echo $linea->nombre; ?> 
			- x<?php echo $linea->unidades; ?> - 
			(<?php echo $linea->precioTotal(); ?> Euros)

			<a href="index.php?contr=<?php echo $contr; ?>&idCompraQuitar=<?php echo $indice; ?>"><span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span></a>
			
			-
			<a href="index.php?contr=<?php echo $contr; ?>&anadirUnidad=<?php echo $indice; ?>">
			<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>

			<a href="index.php?contr=<?php echo $contr; ?>&quitarUnidad=<?php echo $indice; ?>">
			<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>

		</li>

	<?php } ?>
</ul>

<hr>
Total productos: <?php echo $_SESSION['micarro']->totalUnidades(); ?> productos
<hr>
<a href="index.php?contr=usuariosController.php&accion=vercesta">Ver cesta</a>