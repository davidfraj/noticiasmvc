<?php  

//Llamamos a los MODELOS
// require('modulos/carrito/models/LineaCarritoModel.php');
// require('modulos/carrito/models/carritoModel.php');

//Llamamos a posibles variables globales
global $contr;

//Me creo un objeto de la clase Carrito
//$micarro, deberia ser una variable de SESSION
//TRASLADADO a index.php, JUSTO DESPUES DE session_start()
// if(!isset($_SESSION['micarro'])){
// 	$_SESSION['micarro']=new Carrito();
// }

//Que pasa si quiero incrementar el numero de unidades
if(isset($_GET['anadirUnidad'])){
	$_SESSION['micarro']->incrementa($_GET['anadirUnidad']);
}

//Que pasa si quiero incrementar el numero de unidades
if(isset($_GET['quitarUnidad'])){
	$_SESSION['micarro']->decrementa($_GET['quitarUnidad']);
}

//Que pasa si el usuario quiere comprar
if(isset($_GET['idCompra'])){
	$_SESSION['micarro']->agregar($_GET['idCompra']);
}

if(isset($_GET['idCompraQuitar'])){
	$_SESSION['micarro']->quitar($_GET['idCompraQuitar']);
}

//Le doy a la vista, los datos del carrito
$lineasmicarro=$_SESSION['micarro']->listado();

//Llamar a la vista
require('modulos/carrito/views/carritoView.php');


?>