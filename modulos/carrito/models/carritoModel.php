<?php  
class Carrito{
	public $elementos; //Array de Lineas de Carrito

	public function __construct(){
		$this->elementos=[]; //Inicializo el array
	}

	public function agregar($id, $unidades=1){
		//Busco en mis elementos, a ver si esta el id
		$coincide=-1;

		for($i=0;$i<count($this->elementos);$i++){
			if($this->elementos[$i]->id == $id){
				$coincide=$i;
			}
		}

		if($coincide!=-1){
			$this->elementos[$coincide]->unidades+=$unidades;
		}else{
			$this->elementos[]=new LineaCarrito($id, $unidades);
		}

	}

	public function quitar($posicion){
		// Borra un elemento, desde la posicion indicada, en el vector $this->elementos
		array_splice($this->elementos, $posicion, 1);
	}

	public function listado(){
		return $this->elementos;
	}

	public function totalUnidades(){
		$total=0;
		for($i=0;$i<count($this->elementos);$i++){
			$total+=$this->elementos[$i]->unidades;
		}
		return $total;
	}

	public function incrementa($posicion){
		$this->elementos[$posicion]->unidades++;
	}

	public function decrementa($posicion){
		$this->elementos[$posicion]->unidades--;
		if($this->elementos[$posicion]->unidades==0){
			$this->quitar($posicion);
		}
	}	

}
?>