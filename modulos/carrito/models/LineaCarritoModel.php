<?php  
class LineaCarrito{

	public $id;
	public $nombre;
	public $precio;
	public $unidades;
	public $conexion;

	public function __construct($id, $unidades=1){
		//Relleno datos:
		$this->id=$id;
		$this->unidades=$unidades;
		$this->conexion=Conexion::conectar();
		//Conecto a bbdd, y me traigo nombre y precio
		$sql="SELECT * FROM productos WHERE idPro=$id";
		$consulta=$this->conexion->query($sql);
		$registro=$consulta->fetch_array();
		$this->nombre=$registro['nombrePro'];
		$this->precio=$registro['precioPro'];
	}

	public function precioTotal(){
		return $this->precio*$this->unidades;
	}

}
?>