<?php  
class LineaPedido{

	public $id;
	public $nombre;
	public $precio;
	public $unidades;
	public $conexion;

	public function __construct($idProductoPedido){
		//Relleno datos:
		$this->id=$idProductoPedido;
		$this->conexion=Conexion::conectar();
		//Conecto a bbdd, y me traigo nombre y precio
		$sql="SELECT * FROM productos_pedidos INNER JOIN productos ON productos_pedidos.idProducto=productos.idPro WHERE idProductoPedido=$idProductoPedido";
		$consulta=$this->conexion->query($sql);
		$registro=$consulta->fetch_array();
		$this->nombre=$registro['nombrePro'];
		$this->precio=$registro['precioProductoPedido'];
		$this->unidades=$registro['cantidadProductoPedido'];
	}

	public function precioTotal(){
		return $this->precio*$this->unidades;
	}

}
?>