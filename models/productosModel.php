<?php  

require('models/repositorioModel.php');
class Productos extends Repositorio{

	//conexion, datos, tabla, id ()

	public function __construct(){
		parent::__construct(); //constructor del padre
		$this->tabla='productos';
		$this->id='idPro';
		$this->model='Producto'; //nombre de la clase
	}

	public function listado($palabra=''){
		$sql="SELECT * FROM ".$this->tabla." WHERE nombrePro LIKE '%$palabra%'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){

			//$dato = new Noticia($registro);
			$dato = new $this->model($registro);
			$this->datos[]=$dato;

		}
		return $this->datos;
	}

}
?>