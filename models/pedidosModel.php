<?php 

class Pedidos{

	private $conexion;
	private $id;
	private $pedidos;

	public function __construct($id){
		$this->conexion=Conexion::Conectar();
		$this->id=$id;
		$this->pedidos=[]; //Inicializo el array
	}

	public function listar(){
		$sql="SELECT * FROM pedidos WHERE usuarioPedido=".$this->id;
		$consulta=$this->conexion->query($sql);
		while($p=$consulta->fetch_array()){
			$pedido=new Pedido($p['idPedido'], $p['fechaPedido']);
			$this->pedidos[]=$pedido;
		}
		return $this->pedidos;
	}


}

?>