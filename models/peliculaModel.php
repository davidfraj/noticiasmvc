<?php  
class Pelicula{

	public $id;
	public $titulo;
	public $texto;
	public $imagen;

	public function __construct($registro){
		$this->id=$registro['idPel'];
		$this->titulo=$registro['tituloPel'];
		$this->texto=$registro['textoPel'];
		$this->imagen=$registro['imagenPel'];
	}

}