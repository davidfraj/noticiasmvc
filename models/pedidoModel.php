<?php  
class Pedido{

	private $conexion;
	public $idPedido;
	public $fechaPedido;
	public $lineasPedido;

	public function __construct($idPedido=0, $fechaPedido=0){
		$this->conexion=Conexion::Conectar();
		$this->idPedido=$idPedido;
		$this->fechaPedido=$fechaPedido;
		$this->lineasPedido=[]; 
		
		//Si $idPedido es distinto de 0, relleno los productos de dicho pedido
		if($idPedido!=0){
			$sql="SELECT * FROM productos_pedidos WHERE idPedido=$idPedido";
			$consulta=$this->conexion->query($sql);
			while($linea=$consulta->fetch_array()){
				$lin=new LineaPedido($linea['idProductoPedido']);
				$this->lineasPedido[]=$lin;
			}
		}
	}

	public function guardar($idUsuario, $cesta){
		//Genero la fecha
		$fechaPedido=time();
		//Establezco consulta
		$sql="INSERT INTO pedidos(usuarioPedido, fechaPedido)VALUES($idUsuario, $fechaPedido)";
		//Ejecuto consulta
		$this->conexion->query($sql);
		//Obtengo el ultimo autonumerico creado
		$ultimoIdPedido=$this->conexion->insert_id;
		//Recorro la $cesta, y por cada producto
		//en la cesta, agrego un registro a la tabla
		// de productos_pedidos
		foreach ($cesta->listado() as $linea) {
			$idProducto=$linea->id;
			$cantidadProductoPedido=$linea->unidades;
			$precioProductoPedido=$linea->precio;
			$sql="INSERT INTO productos_pedidos(idPedido, idProducto, cantidadProductoPedido, precioProductoPedido)VALUES($ultimoIdPedido, $idProducto, $cantidadProductoPedido, $precioProductoPedido)";
			$this->conexion->query($sql);
		}
		return true;
	} //Fin del metodo GUARDAR

	public function totalPedido(){
		$total=0;
		foreach ($this->lineasPedido as $linea) {
			$total+=$linea->precioTotal();
		}
		return $total;
	}

}



?>