<?php  

require('models/repositorioModel.php');
class Peliculas extends Repositorio{

	//conexion, datos, tabla, id ()

	public function __construct(){
		parent::__construct(); //constructor del padre
		$this->tabla='peliculas';
		$this->id='idPel';
		$this->model='Pelicula'; //nombre de la clase
	}

	public function listado($palabra=''){
		$sql="SELECT * FROM ".$this->tabla." WHERE tituloPel LIKE '%$palabra%'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){

			//$dato = new Noticia($registro);
			$dato = new $this->model($registro);
			$this->datos[]=$dato;

		}
		return $this->datos;
	}

}
?>