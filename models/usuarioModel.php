<?php 
class Usuario{

	private $conexion;
	public $id;
	public $nombre;
	public $correo;
	public $nivel;

	public function __construct(){
		$this->conexion=Conexion::conectar();
		$this->id='';
		$this->nombre='';
		$this->correo='';
		$this->nivel='';
	}

	public function registrar($correo, $nombre, $clave, $clave2){
		$datosCorrectos=true;
		$datosMensaje='';
		
		//Compruebo el nombre 
		if(strlen($nombre)<3){
			$datosCorrectos=false;
			$datosMensaje.='El nombre debe tener mas de 3 caracteres';
		}

		//Compruebo que el correo no exits
		$sql="SELECT correoUsuario FROM usuarios WHERE correoUsuario='$correo'";
		$consulta=$this->conexion->query($sql);
		if($fila=$consulta->fetch_array()){
			$datosCorrectos=false;
			//Aqui pueden pasar dos cosas, que el correo del usuario, exista y su estadoUsuario este a 0 o a 1
			if($fila['estadoUsuario']==1){
				$datosMensaje.='El correo ya existe';
			}else{
				$datosMensaje.='Hemos visto que ya has estado antes con nosotros. Te hemos enviado un email a tu cuenta para REACTIVAR tu usuario en nuestro sistema. Gracias por todo';
				//TODO: ENVIAR CORREO DE ACTIVACION
			}

		}

		//Compruebo que la clave es correcta
		if(strlen($clave)>=8){
			if($clave!=$clave2){
				$datosCorrectos=false;
				$datosMensaje.='las claves no coinciden';
			}
		}else{
			$datosCorrectos=false;
			$datosMensaje.='la clave debe tener al menos 8 caracteres';
		}

		//Si los datos son correctos, seguimos y sino return false
		if($datosCorrectos){
			$fecha=time();
			$sql="INSERT INTO usuarios(nombreUsuario, correoUsuario, claveUsuario, fechaUsuario)VALUES('$nombre', '$correo', md5('$clave'), '$fecha')";
			$consulta=$this->conexion->query($sql);

			//En este punto, se podria enviar un correo
			//De bienvenida al usuario

			//ENVIANDO CORREO CON PHPMAILER


			$mail = new PHPMailer\PHPMailer\PHPMailer(true);
			try {
			    //Server settings
			    $mail->SMTPDebug = 2;
			    $mail->isSMTP();
			    $mail->Host = 'smtp.MIDOMINIO.COM';
			    $mail->SMTPAuth = true;
			    $mail->Username = 'admin@MIDOMINIO.com';
			    $mail->Password = 'PASS DE correo';
			    $mail->SMTPSecure = 'tls';
			    $mail->Port = 587;

			    //Recipients
			    $mail->setFrom('davidfraj@gmail.com', 'David Fraj Blesa');
			    $mail->addAddress($correo, $nombre);     // Add a recipient
			    //$mail->addAddress('ellen@example.com');               // Name is optional
			    //$mail->addReplyTo('info@example.com', 'Information');
			    //$mail->addCC('cc@example.com');
			    //$mail->addBCC('bcc@example.com');

			    //Attachments
			    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			    //Content
			    $mail->isHTML(true);                                  // Set email format to HTML
			    $mail->Subject = 'Bienvenido a la web';
			    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			    $mail->send();
			    echo 'Message has been sent';
			} catch (Exception $e) {
			    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
			}

			//FIN DE ENVIAR CORREO CON PHPMAILER

			return true;
		}else{
			echo $datosMensaje;
			return false; //TEMPORAL -> Ya veremos a ver.....
		}

	}

	public function comprobar($correo, $clave){
		$sql="SELECT * FROM usuarios WHERE correoUsuario='$correo' AND claveUsuario=md5('$clave') AND estadoUsuario=1";
		$consulta=$this->conexion->query($sql);
		if($registro=$consulta->fetch_array()){
			//correcto, RELLENAMOS EL USUARIO
			$this->id=$registro['idUsuario'];
			$this->nombre=$registro['nombreUsuario'];
			$this->correo=$registro['correoUsuario'];
			$this->nivel=$registro['nivelUsuario'];
			return true;
		}else{
			return false;
		}
	}

	public function guardar(){
		$sql="UPDATE usuarios SET correoUsuario='".$this->correo."', nombreUsuario='".$this->nombre."' WHERE idUsuario='".$this->id."'";
		//echo $sql;
		$consulta=$this->conexion->query($sql);
	}

	public function guardarClave($clave){
		$clave=md5($clave);
		$sql="UPDATE usuarios SET claveUsuario='".$clave."' WHERE idUsuario='".$this->id."'";
		//echo $sql;
		$consulta=$this->conexion->query($sql);
	}

}

?>