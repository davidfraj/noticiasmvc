<?php  
//Incluyo los paquetes de composer
require('vendor/autoload.php');

//Llamo a los namespace si fuera necesario
use Endroid\QrCode\QrCode;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Require de la configuracion de la web
require('includes/config.php');

//Require de funciones utiles
require('includes/funciones.php');

//Require de las classes GENERALES de la web
require('includes/classes/conexion.class.php');

// ESTO NORMALMENTE SE HACE DESDE EL CONTROLADOR del MODULO
// PERO, LO NECESITO PARA MIS VARIABLES DE SESSION
require('modulos/carrito/models/LineaCarritoModel.php');
require('modulos/carrito/models/carritoModel.php');
require('models/usuarioModel.php');

//Le digo que quiero usar variables de session
session_start();

    //Si NO EXISTE $_SESSION['usuarioconectado'], DEBO crearla
    if(!isset($_SESSION['usuarioconectado'])){
      if(!isset($_COOKIE['usuarioconectado'])){
        $_SESSION['usuarioconectado']=null;
        //echo 'SERIALIZAMOS USUARIO';
      }else{
        $_SESSION['usuarioconectado']=unserialize($_COOKIE['usuarioconectado']);
        //echo 'UNSERIALIZE USUARIO';
      }
    }

    //SI NO EXISTE $_SESSION['micarro'], PUEDEN PASAR 2 COSAS
    //Que tengamos una cookie llamada 'micarro' o NO.
    if(!isset($_SESSION['micarro'])){
      if(!isset($_COOKIE['micarro'])){
        $_SESSION['micarro']=new Carrito();
        //echo 'NEW CARRITO';
      }else{
        $_SESSION['micarro']=unserialize($_COOKIE['micarro']);
        //echo 'UNSERIALIZE';
      }
    }

    //Para meter el OBJETO DE $_SESSION['micarro'] en una cookie
    //convertir OBJETO en TEXTO
    $cadenaCarrito=serialize($_SESSION['micarro']);
    //lo meto en cookie
    setcookie('micarro', $cadenaCarrito, time()+(60*60*24));

    //convertir texto en OBJETO
    //$otraVezMiCarro=unserialize($cadenaCarrito);


//Como estoy en el contrador PRINCIPAL
//Desde aqui, voy a recoger el controlador que cargare
if(isset($_GET['contr'])){
  $contr=$_GET['contr'];
}else{
  $contr='noticiasController.php';
}

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Noticias - MVC - MODIFICADO</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

  </head>
  <body>
    <section class="container">

      <header>
        <h1>Noticias - MVC</h1>
        <hr>
      </header>

      <div class="text-right">

      <?php if($_SESSION['usuarioconectado']){ ?>

      Bienvenido <?php echo $_SESSION['usuarioconectado']->nombre; ?> (Nivel <?php echo $_SESSION['usuarioconectado']->nivel; ?>)
      <a href="index.php?contr=usuariosController.php&accion=desconectar">Desconectar</a>
    
      -

      <a href="index.php?contr=usuariosController.php&accion=modificar">Editar perfil</a>

      -

      <a href="index.php?contr=usuariosController.php&accion=pedidos">Pedidos realizados</a>

      <?php if(($_SESSION['usuarioconectado']->nivel)>=9){ ?>
      - <a href="index.php?contr=usuariosController.php&accion=administrar">Administrar</a>
      <?php } ?>

      <?php }else{ ?>

        <a href="index.php?contr=usuariosController.php&accion=identificate">Identificate</a>
        - 
        <a href="index.php?contr=usuariosController.php&accion=registrate">Registrate</a>

      <?php } ?>

      </div>

      <hr>
      <?php cargarModulo('menu'); ?>
      <hr>

      <section id="principal" class="row">
        <div class="col-md-8">
          <?php require('controllers/'.$contr); ?>
        </div>
        <div class="col-md-4">
          <?php cargarModulo('carrito'); ?>
          <hr>
          <?php cargarModulo('banner'); ?>
        </div>
      </section>

      <footer>
        <hr>
        <h4>
          Gracias por su visita
          -
          <small>
            <?php cargarModulo('copyright'); ?>
          </small>
          </h4>
      </footer>

    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/propio.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
      $( "#accordion" ).accordion();
    } );

    $('#palabra').keyup(function(){

        var parametros = {
                "palabra" : $('#palabra').val()
        };
        $.ajax({
                data:  parametros,
                url:   'ejemplo_ajax_proceso.php',
                type:  'post',
                beforeSend: function () {
                        $("#resultados").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultados").html(response);
                },
                error: function(error){
                       $("#resultados").html(error);
                }
        });

    }); //Fin de KEYUP

    </script>

  </body>
</html>