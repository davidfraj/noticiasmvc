<h2>Registro de usuario</h2>
<hr>

<form action="index.php?contr=<?php echo $contr; ?>&accion=registrate" method="post" role="form" enctype="multipart/form-data">

	<div class="form-group">
		<label for="nombre">nombre de usuario:</label>
		<input type="text" class="form-control" name="nombre">
	</div>

	<div class="form-group">
		<label for="correo">correo de usuario:</label>
		<input type="email" class="form-control" name="correo">
	</div>

	<div class="form-group">
		<label for="clave">clave de usuario:</label>
		<input type="password" class="form-control" name="clave">
	</div>

	<div class="form-group">
		<label for="clave2">repite clave de usuario:</label>
		<input type="password" class="form-control" name="clave2">
	</div>
	
	<div class="form-group">
		<input type="submit" name="enviar" value="Enviar">
	</div>
</form>