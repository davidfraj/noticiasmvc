<h2>Listado de noticias</h2>

<hr>

<?php if($_SESSION['usuarioconectado']){ ?>
<?php if(($_SESSION['usuarioconectado']->nivel)>=1){ ?>

	<a href="index.php?contr=<?php echo $contr; ?>&accion=insertar">Alta de noticia</a>

<?php }} ?>

<hr>

<?php foreach ($misnoticias as $noticia) { ?>

<article>	
	<header>
		<h2>
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $noticia->id;?>&accion=ver">
				<?php echo $noticia->titulo; ?>
			</a>

			<?php if($_SESSION['usuarioconectado']){ ?>
			<?php if(($_SESSION['usuarioconectado']->nivel)>=5){ ?>

			-
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $noticia->id;?>&accion=borrar">Borrar</a>
			-
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $noticia->id;?>&accion=modificar">Modificar</a>

			<?php }} ?>

		</h2>
	</header>
	<section><?php echo $noticia->texto; ?></section>
	<footer><?php echo timestampToFecha($noticia->fecha); ?></footer>
</article>
<hr>

<?php } ?>