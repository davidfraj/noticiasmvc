<h2>Listado de peliculas - <?php echo $titulo; ?></h2>
<hr>

<form action="index.php?contr=<?php echo $contr; ?>&accion=<?php echo $formAccion; ?>" method="post" role="form" enctype="multipart/form-data">
	<div class="form-group">
		<label for="titulo">Titulo de la pelicula:</label>
		<input type="text" class="form-control" name="titulo" value="<?php echo $formTitulo; ?>">
	</div>
	<div class="form-group">
		<label for="texto">Texto de la pelicula:</label>
		<textarea name="texto" id="" cols="30" rows="10" class="form-control"><?php echo $formTexto; ?></textarea>
	</div>
	<div class="form-group">
		<label for="imagen">Imagen de la pelicula:</label>
		<input type="file" class="form-control" value="<?php echo $formImagen; ?>" name="imagen">
	</div>
	<div class="form-group">
		<input type="hidden" name="id" value="<?php echo $formId; ?>">
		<input type="submit" name="enviar" value="enviar">
	</div>
</form>