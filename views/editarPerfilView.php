<h2>Editar perfil del usuario</h2>
<hr>

<?php if($muestraMensaje){ ?>
	
	<div class="alert alert-<?php echo $tipoMensaje;?> alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong>Resultado de la accion</strong> <?php echo $textoMensaje; ?>
	</div>

<?php } ?>


<form action="index.php?contr=<?php echo $contr; ?>&accion=modificar" method="post" role="form" enctype="multipart/form-data">

	<div class="form-group">
		<label for="nombre">nombre de usuario:</label>
		<input type="text" class="form-control" name="nombre" value="<?php echo $_SESSION['usuarioconectado']->nombre; ?>">
	</div>

	<div class="form-group">
		<label for="correo">correo de usuario:</label>
		<input type="email" class="form-control" name="correo" value="<?php echo $_SESSION['usuarioconectado']->correo; ?>" readonly>
	</div>

	<div class="form-group">
		<label for="clave">clave de usuario:</label>
		<input type="password" class="form-control" name="clave">
	</div>

	<div class="form-group">
		<label for="clave2">repite clave de usuario:</label>
		<input type="password" class="form-control" name="clave2">
	</div>
	
	<div class="form-group">
		<input type="submit" name="enviar" value="Enviar">
	</div>
</form>