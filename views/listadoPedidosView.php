<h2>Listado de pedidos anteriores</h2>
<hr>
<div id="accordion">
<?php foreach ($mispedidos as $pedido) { ?>
	<h3>
		<?php echo $pedido->idPedido; ?>
		 - 
		<?php echo timestampToFecha($pedido->fechaPedido); ?>
		-
		Click aqui para ver mas detalles <span class="glyphicon glyphicon-arrow-down"></span>
	</h3>
	<div>
		<ul>
			<?php foreach($pedido->lineasPedido as $linea){ ?>
				<li>
					<?php echo $linea->nombre; ?>
					-
					<?php echo $linea->precio; ?>
					-
					<?php echo $linea->unidades; ?>	
					-
					<?php echo $linea->precioTotal(); ?>
				</li>
			<?php } ?>
		</ul>
		<footer class="text-right">Total de pedido: <?php echo $pedido->totalPedido(); ?> Euros</footer>
	</div>
<?php } ?>
</div>