<h2>Listado de peliculas</h2>

<hr>
<a href="index.php?contr=<?php echo $contr; ?>&accion=insertar">Alta de peliculas</a>
<hr>

<?php foreach ($mispeliculas as $pelicula) { ?>

<article>	
	<header>
		<h2>
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $pelicula->id;?>&accion=ver">
				<?php echo $pelicula->titulo; ?>
			</a>
			-
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $pelicula->id;?>&accion=borrar">Borrar</a>
			-
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $pelicula->id;?>&accion=modificar">Modificar</a>
		</h2>
	</header>
	<section><?php echo $pelicula->texto; ?></section>
	<footer><?php echo $pelicula->imagen; ?></footer>
</article>
<hr>

<?php } ?>