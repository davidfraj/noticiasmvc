<h2>Listado de productos - <?php echo $titulo; ?></h2>
<hr>

<form action="index.php?contr=<?php echo $contr; ?>&accion=<?php echo $formAccion; ?>" method="post" role="form" enctype="multipart/form-data">
	<div class="form-group">
		<label for="nombre">Nombre de producto:</label>
		<input type="text" class="form-control" name="nombre" value="<?php echo $formNombre; ?>">
	</div>
	
	<div class="form-group">
		<label for="precio">precio de producto:</label>
		<input type="text" class="form-control" name="precio" value="<?php echo $formPrecio; ?>">
	</div>

	<div class="form-group">
		<label for="unidades">unidades de producto:</label>
		<input type="text" class="form-control" name="unidades" value="<?php echo $formUnidades; ?>">
	</div>

	<div class="form-group">
		<label for="fecha">Fecha de la noticia:</label>
		<input type="date" class="form-control" value="<?php echo $formFecha; ?>" name="fecha">
	</div>
	
	<div class="form-group">
		<input type="hidden" name="id" value="<?php echo $formId; ?>">
		<input type="submit" name="enviar" value="enviar">
	</div>
</form>