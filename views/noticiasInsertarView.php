<h2>Listado de noticias - <?php echo $titulo; ?></h2>
<hr>

<form action="index.php?contr=<?php echo $contr; ?>&accion=<?php echo $formAccion; ?>" method="post" role="form" enctype="multipart/form-data">
	<div class="form-group">
		<label for="titulo">Titulo de la noticia:</label>
		<input type="text" class="form-control" name="titulo" value="<?php echo $formTitulo; ?>">
	</div>
	<div class="form-group">
		<label for="texto">Texto de la noticia:</label>
		<textarea name="texto" id="" cols="30" rows="10" class="form-control"><?php echo $formTexto; ?></textarea>
	</div>
	<div class="form-group">
		<label for="fecha">Fecha de la noticia:</label>
		<input type="date" class="form-control" value="<?php echo $formFecha; ?>" name="fecha">
	</div>
	<div class="form-group">
		<input type="hidden" name="id" value="<?php echo $formId; ?>">
		<input type="submit" name="enviar" value="enviar">
	</div>
</form>