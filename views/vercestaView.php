<h2>Tu cesta de la compra - <small>El carrito</small></h2>

<?php foreach ($lineasmicarro as $linea) { ?>

	<article>
		<h3><?php echo $linea->nombre; ?></h3>
		<p>
			<?php echo $linea->unidades; ?>
			 - 
			<?php echo $linea->precio; ?>
			 - 
			<?php echo $linea->precioTotal(); ?>	
		</p>
	</article>
	<hr>

<?php } ?>

<a href="index.php?contr=usuariosController.php&accion=finalizarcompra">
	Finalizar la compra
</a>