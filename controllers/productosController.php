<?php  
// fichero controllers/productosController.php

//Llamara a los modelos
require('models/productoModel.php');
require('models/productosModel.php');

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Procesara las ordenes del usuario
$elementos=new Productos();

//Dependiendo de la accion que reciba el controlador
//hago una cosa u otra
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Creo un switch, para seleccionar la accion
switch($accion){
	case 'listado':
		$palabra='';
		if(isset($_POST['buscar'])){
			$palabra=$_POST['palabra'];
		}
		$miselementos=$elementos->listado($palabra);
		$vista='productosView.php';
		break;

	case 'listadojson':
		$miselementos=$elementos->listado();
		$json=json_encode($miselementos);
		$vista='productosJsonView.php';
		break;

	case 'ver':
		$miselementos=$elementos->detalle($_GET['id']);
		$vista='productosView.php';
		break;

	case 'insertar':

		$vista='productosInsertarView.php';
		$titulo='Insertar Producto';

		$formId='';
		$formNombre='';
		$formPrecio='';
		$formUnidades='';
		$formFecha=date('Y-m-d'); //YYYY-MM-DD;
		$formAccion='insercion';

		break;

	case 'insercion':
		//Registro es un ARRAY asociativo, con los mismos nombres
		//que la tabla y el ORDEN !!!!!!!!!!!!!!!
		$registro['nombrePro']=$_POST['nombre'];
		$registro['precioPro']=$_POST['precio'];
		$registro['unidadesPro']=$_POST['unidades'];
		$registro['fechaPro']=fechaToTimestamp($_POST['fecha']);
		//llamo al metodo para insertar el registro
		if($elementos->insertar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'borrar':
		if($elementos->borrar($_GET['id'])){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'modificar':
		$elelemento=$elementos->detalle($_GET['id'])[0];
		$vista='productosInsertarView.php';
		$titulo='Modificar Producto';

		$formId=$elelemento->id;
		$formNombre=$elelemento->nombre;
		$formPrecio=$elelemento->precio;
		$formUnidades=$elelemento->unidades;
		$formFecha=date('Y-m-d',$elelemento->fecha); //YYYY-MM-DD;
		$formAccion='modificacion';

		break;

	case 'modificacion':
		$registro['idPro']=$_POST['id'];
		$registro['nombrePro']=$_POST['nombre'];
		$registro['precioPro']=$_POST['precio'];
		$registro['unidadesPro']=$_POST['unidades'];
		$registro['fechaPro']=fechaToTimestamp($_POST['fecha']);

		if($elementos->modificar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'imprimir':
		$miselementos=$elementos->detalle($_GET['id']);

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$vista='productosPdfView.php';
		break;

	case 'excel':

		$miselementos=$elementos->listado();

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Nombre del producto');
		$sheet->setCellValue('B1', 'Precio');
		$sheet->setCellValue('C1', 'Unidades');
		$sheet->setCellValue('D1', 'Fecha');

		$fila=2;
		foreach($miselementos as $elemento){
			$sheet->setCellValue('A'.$fila, $elemento->nombre);
			$sheet->setCellValue('B'.$fila, $elemento->precio);
			$sheet->setCellValue('C'.$fila, $elemento->unidades);
			$sheet->setCellValue('D'.$fila, date('d/n/Y',$elemento->fecha));
			$fila++;
		}

		$writer = new Xlsx($spreadsheet);
		$writer->save('productos.xlsx');

		$vista='productosView.php';

		break;

}

//Pintara las vistas
require('views/'.$vista);

?>