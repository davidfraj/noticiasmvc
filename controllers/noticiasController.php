<?php  
// fichero controllers/noticiasController.php

//Llamara a los modelos
require('models/noticiaModel.php');
require('models/noticiasModel.php');

//Procesara las ordenes del usuario
$elementos=new Noticias();

//Dependiendo de la accion que reciba el controlador
//hago una cosa u otra
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Creo un switch, para seleccionar la accion
switch($accion){
	case 'listado':
		$misnoticias=$elementos->listado();
		$vista='noticiasView.php';
		break;

	case 'ver':
		$misnoticias=$elementos->detalle($_GET['id']);
		$vista='noticiasView.php';
		break;

	case 'insertar':

		$vista='noticiasInsertarView.php';
		$titulo='Insertar Noticia';

		$formId='';
		$formTitulo='';
		$formTexto='';
		$formFecha=date('Y-m-d'); //YYYY-MM-DD;
		$formAccion='insercion';

		break;

	case 'insercion':
		//Registro es un ARRAY asociativo, con los mismos nombres
		//que la tabla y el ORDEN !!!!!!!!!!!!!!!
		$registro['tituloNot']=$_POST['titulo'];
		$registro['textoNot']=$_POST['texto'];
		$registro['fechaNot']=fechaToTimestamp($_POST['fecha']);
		//llamo al metodo para insertar el registro
		if($elementos->insertar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'borrar':
		if($elementos->borrar($_GET['id'])){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'modificar':
		$lanoticia=$elementos->detalle($_GET['id'])[0];
		$vista='noticiasInsertarView.php';
		$titulo='Modificar Noticia';

		$formId=$lanoticia->id;
		$formTitulo=$lanoticia->titulo;
		$formTexto=$lanoticia->texto;
		$formFecha=date('Y-m-d',$lanoticia->fecha);
		$formAccion='modificacion';

		break;

	case 'modificacion':
		$registro['idNot']=$_POST['id'];
		$registro['tituloNot']=$_POST['titulo'];
		$registro['textoNot']=$_POST['texto'];
		$registro['fechaNot']=fechaToTimestamp($_POST['fecha']);

		if($elementos->modificar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

}

//Pintara las vistas
require('views/'.$vista);

?>