<?php  
//Llama a los modelos
//Estoy llamando al carritoModel y lineaCarritoModel
// desde el index. Asi que no hace falta desde aqui
require('models/lineaPedidoModel.php');
require('models/pedidoModel.php');
require('models/pedidosModel.php');

//Crear objetos usando los modelos
//Me creo un objeto de la clase Carrito
//$micarro, deberia ser una variable de SESSION
if(!isset($_SESSION['micarro'])){
	$_SESSION['micarro']=new Carrito();
}

//Recibe la accion del usuario
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='vercesta';
}

//Llama a la vista adecuada
switch($accion){
	case 'vercesta':
		//Le doy a la vista, los datos del carrito
		$lineasmicarro=$_SESSION['micarro']->listado();
		$vista='vercestaView.php';
		break;

	case 'identificate':
		if(isset($_POST['enviar'])){
			$usuario=new Usuario();
			if($usuario->comprobar($_POST['correo'], $_POST['clave'])){
				//Me creo una variable de SESSION con el usuario
				$_SESSION['usuarioconectado']=$usuario;
				if(isset($_POST['recordar'])){
					setcookie('usuarioconectado', serialize($usuario), time()+(60*60*24));
				}
				header('location: index.php');
				//$vista='identificateConfirmacionView.php';
			}else{
				$vista='identificateView.php';
			}
		}else{
			$vista='identificateView.php';
		}
		break;

	case 'registrate':
		if(isset($_POST['enviar'])){
			// AQUI ME CREO EL USUARIO
			$usuario=new Usuario();
			if($usuario->registrar($_POST['correo'], $_POST['nombre'], $_POST['clave'], $_POST['clave2'])){
				$vista='registrateConfirmacionView.php';
			}else{
				//Si los datos no son validos
				//mando al usuario al formulario de registro
				$vista='registrateView.php';
			}

		}else{
			//Pinto el formulario para registrarse
			$vista='registrateView.php';
		}
		break;

	case 'desconectar':
		$_SESSION['usuarioconectado']=null;
		setcookie('usuarioconectado',null,0);
		header('location:index.php');
		break;

	case 'modificar':
		$vista='editarPerfilView.php';
		
		$muestraMensaje=false;
		$tipoMensaje='';
		$textoMensaje='';

		if(isset($_POST['enviar'])){

			$muestraMensaje=true;

			//Si estamos en este caso.... guardamos datos
			//En la variable de session
			$nombre=$_POST['nombre'];
			$correo=$_POST['correo'];

			$usu=new Usuario();
			$usu->id=$_SESSION['usuarioconectado']->id;
			$usu->nombre=$nombre;
			$usu->correo=$correo;

			//En la bbdd
			$usu->guardar();

			$tipoMensaje='success';
			$textoMensaje.='Sus datos han sido actualizados con exito<br>';

			$clave=$_POST['clave'];
			if(strlen($clave)>0){
				if(strlen($clave)>=8){
					$clave2=$_POST['clave2'];
					if($clave==$clave2){
						$usu->guardarClave($clave);
						$textoMensaje.='su clave ha sido actualizada con exito<br>';
					}else{
						$tipoMensaje='danger';
						$textoMensaje.='Las contraseñas no coinciden<br>';
					}
				}else{
					$tipoMensaje='danger';
					$textoMensaje.='La clave debe ser al menos 8 caracteres<br>';
				}
			}
		}

		break;

	case 'finalizarcompra':
		//CREAMOS UN NUEVO PEDIDO
		$pedido=new Pedido();

		//A ESE PEDIDO LE PASAREMOS LOS DATOS DEL USUARIO CONECTADO
		//AL PEDIDO LE PASAREMOS LOS PRODUCTOS EN LA CESTA
		$r=$pedido->guardar($_SESSION['usuarioconectado']->id, $_SESSION['micarro']);

		//SI TODO VA BIEN, LANZAMOS LA VISTA ADECUADA
		if($r==true){
			//Ha ido todo bien
			$_SESSION['micarro']=new Carrito();
		}else{
			//Tenemos un problema
		}
		$vista='finalizarCompraView.php';

		break;

	case 'pedidos':

		$pedidos=new Pedidos($_SESSION['usuarioconectado']->id);

		$mispedidos=$pedidos->listar();

		$vista='listadoPedidosView.php';
		
		break;


} //fin de switch

require('views/'.$vista);
?>