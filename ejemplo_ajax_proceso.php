<?php  

//Require de la configuracion de la web
require('includes/config.php');

//Require de funciones utiles
require('includes/funciones.php');

//Require de las classes GENERALES de la web
require('includes/classes/conexion.class.php');

//Llamara a los modelos
require('models/productoModel.php');
require('models/productosModel.php');

//Procesara las ordenes del usuario
$elementos=new Productos();

//REcojo la $palabra por post, que le paso desde AJAX (ver index.php el final)
$palabra=$_POST['palabra'];

//Relleno mis resultados, llamando al modelo de datos, y a 
//su metodo listado, donde le paso la $palabra de busqueda
$miselementos=$elementos->listado($palabra);

//Ahora llamo a la vista que devolvere......
?>

<?php foreach ($miselementos as $elemento) { ?>

<article>	
	<header>
		<h2>
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $elemento->id;?>&accion=ver">
			
			<img src="imagenes/qrproducto_<?php echo $elemento->id;?>.png" alt="<?php echo $elemento->nombre; ?>" width="100">
			</a>

			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $elemento->id;?>&accion=ver">
				<?php echo $elemento->nombre; ?>
			</a>
		</h2>
	</header>
	<section><?php echo $elemento->precio; ?></section>
	<footer>
		<?php echo $elemento->unidades; ?>
		
		<?php echo timestampToFecha($elemento->fecha); ?>
	</footer>
</article>
<hr>

<?php } ?>